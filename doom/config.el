;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
;; they are implemented.


;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "bezy1"
      user-mail-address "bezy1@protonmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
(setq doom-font (font-spec :family "DejaVu Sans Mono" :size 13 :weight 'bold))
;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:

(setq doom-theme 'doom-dracula)
;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys


;; Requires
(require 'latex)
(require 'emmet-mode)
(require 'helm-mode)
(require 'json-mode)
(require 'racket-mode)
; Hooks
(add-hook 'js2-mode-hook 'skewer-mode)
(add-hook 'css-mode-hook 'skewer-css-mode)
(add-hook 'html-mode-hook 'skewer-html-mode)
(add-hook 'org-hook-mode
 (lambda ()
   (push  '("\and" . ?∧) prettify-symbols-alist)
   (push  '("\or" . ?∨) prettify-symbols-alist)
   (push  '("\ge" . ?≥) prettify-symbols-alist)
   (push  '("\le" . ?≤) prettify-symbols-alist)
   (push  '("\bi". ?⟺) prettify-symbols-alist)
   (push  '("\ne" . ?≠) prettify-symbols-alist)))
(add-hook 'racket-mode-hook (
                             lambda ()
                               (push  '("and" . ?∧) prettify-symbols-alist)
                               (push  '("or" . ?∨) prettify-symbols-alist)
                               (push  '(">=" . ?≥) prettify-symbols-alist)
                               (push  '("<=" . ?≤) prettify-symbols-alist)
                               (push  '("lambda" . ?λ) prettify-symbols-alist)))
;; Editor Setting
(set-frame-parameter (selected-frame) 'alpha '(85 85))
(add-to-list 'default-frame-alist '(alpha 85 85))
(set-face-attribute 'default nil :background "black" :foreground "white" :font "DejaVu Sans Mono" :height 130)
(setq-default indent-tabs-mode nil)
(setq-default evil-escape-key-sequence "jj")
(setq-default evil-escape-delay 0.2)
(setq org-pretty-entities t)
(global-prettify-symbols-mode 1)
(setq +doom-dashboard-banner-dir "~/pics")
(setq +doom-dashboard-banner-file "euler.png")
;;; evil
(define-key evil-normal-state-map "ا" 'evil-backward-char)
(define-key evil-normal-state-map "ت" 'evil-next-line)
(define-key evil-normal-state-map "ن" 'evil-previous-line)
(define-key evil-normal-state-map "م" 'evil-forward-char)
(define-key evil-normal-state-map "ع" 'evil-redo)
(define-key evil-normal-state-map "ي ي" 'evil-delete-whole-line)
(define-key evil-normal-state-map "ء" 'evil-delete-char)
(define-key evil-normal-state-map "ه" 'evil-insert)

;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You ااcan also try 'gd' (or 'C-c c d') to jump to their definition and see how
