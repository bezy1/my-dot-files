syntax enable
filetype indent plugin on
set number
set nowrap
set tw=0
set wm=0
set tabstop=4
set foldmethod=syntax
set foldnestmax=10
set nofoldenable
set foldlevel=2 
set hlsearch
imap jj <ESC>
nnoremap <SPACE> za
set showcmd
set ruler
packadd! dracula
colorscheme dracula
call plug#begin('~/.vim/plugged')

Plug 'jceb/vim-orgmode'
call plug#end()


